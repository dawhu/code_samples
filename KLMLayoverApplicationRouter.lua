--[[
 *  [ Corona SDK - Lua ]
 *  Project: KLM Layover with a local
 *  Url: https://layoverwithalocal.klm.com/
 *
 *  Voor dit project heb ik een uitbreiding gemaakt op Composer (Scene Management library van Corona).
 *  Belangrijkste functionaliteiten die ik mis in composer zijn de history en de mogelijkheid om scenes te queuen.
 *  Deze functionaliteiten zijn dan ook in deze applicationRouter toegevoegd.
 */
]]

Composer = Composer or require( 'composer' );
Composer.recycleOnSceneChange = true;

shcc.router = shcc.router or {};

shcc.router.history = {};
shcc.router.sceneQueue = {};

shcc.router.stage = Composer.stage;

shcc.router.DEFAULT_TO_EFFECT      = 'slideLeft';
shcc.router.DEFAULT_BACK_EFFECT    = 'slideRight';
shcc.router.DEFAULT_EFFECT_TIME    = 400;

shcc.router.DEFAULT_OVERLAY_MODAL        = true;
shcc.router.DEFAULT_OVERLAY_OPEN_EFFECT  = 'fromBottom';
shcc.router.DEFAULT_OVERLAY_CLOSE_EFFECT = 'slideDown';
shcc.router.DEFAULT_OVERLAY_EFFECT_TIME  = 400;

shcc.router.overlayOpen = false;
shcc.router.isTransitioning = false;

--
-- Private variables
--
local _transitionTimer;
local _queueTransitionTimer;

-- Method to navigate to given scene
-- @String: Path to scene file (required)
-- @Object: Object with the following params (optional)
--      |_  effect = String
--      |_  time = Integer
--      |_  params = Object
function shcc.router.navigateTo( path, options )
    options         = options or {};
    options.effect  = options.effect or shcc.router.DEFAULT_TO_EFFECT;
    options.time    = options.time or shcc.router.DEFAULT_EFFECT_TIME;
    options.params  = options.params or {};

    if klm.layover.navigation:isOpen() then
        klm.layover.navigation:close();
        options.effect = 'fade';
        options.time = 350;
    end

    if shcc.router.isTransitioning then
        shcc.router.addSceneToQueue( path, options );
        return false;
    end

    -- Set overlayOpen to false
    shcc.router.overlayOpen = false;

    -- Set transitioning to true
    shcc.router.isTransitioning = true;

    -- Add the new route to the history
    -- Only add if it's not the current page
    if path ~= shcc.router.history[ #shcc.router.history ] then
        if options.history ~= false then
            shcc.router.history[ #shcc.router.history+1 ] = path;
        end

        -- Track page if GoogleAnalytics is present
        if GoogleAnalytics then
            GoogleAnalytics:trackPage( path, klm.layover.trackingManager:getPageTitle( path ) );
        end

        Composer.loadScene( path, false, options );
        timer.performWithDelay( 50, function()
            Composer.gotoScene( path, options );

            if _transitionTimer then timer.cancel( _transitionTimer ); end
            _transitionTimer = timer.performWithDelay( options.time, shcc.router.navigateToNextSceneInQueue );

            Global:dispatchEvent({
                name = Constants.Events.ROUTE_CHANGE,
                route = path
            });

            klm.layover.navigation:close();
        end);
    else
        -- Set transitioning to false
        shcc.router.isTransitioning = false;
    end
end

-- Method to navigate to previous scene
-- @Object: Object with the following params (optional)
--      |_  effect = String
--      |_  time = Integer
--      |_  params = Object
function shcc.router.back( options )
    if shcc.router.overlayOpen then
        shcc.router.closeOverlay();
        return;
    end

    if klm.layover.navigation:isOpen() then
        klm.layover.navigation:close();
    end

    options = options or {};
    options.effect  = options.effect or shcc.router.DEFAULT_BACK_EFFECT;
    options.time    = options.time or shcc.router.DEFAULT_EFFECT_TIME;

    if shcc.router.isTransitioning then
        if shcc.router.getPreviousSceneName() then
            shcc.router.addSceneToQueue( shcc.router.getPreviousSceneName(), options );
        end
        return false;
    end

    if shcc.router.getPreviousSceneName() then
        -- Track page if GoogleAnalytics is present
        if GoogleAnalytics then
            GoogleAnalytics:trackPage( shcc.router.getPreviousSceneName(), klm.layover.trackingManager:getPageTitle( shcc.router.getPreviousSceneName() ) );
        end

        Composer.gotoScene( shcc.router.getPreviousSceneName(), options );

        if _transitionTimer then timer.cancel( _transitionTimer ); end
        _transitionTimer = timer.performWithDelay( options.time, shcc.router.navigateToNextSceneInQueue );

        Global:dispatchEvent({
            name = Constants.Events.ROUTE_CHANGE,
            route = shcc.router.getPreviousSceneName()
        });

        -- Remove the currentScene from the history
        shcc.router.history[ #shcc.router.history ] = nil;
    end
end

-- Method to open an overlay on top of the current scene
-- @String: Path to scene file (required)
-- @Object: Object with the following params (optional)
--      |_  modal = Boolean
--      |_  effect = String
--      |_  time = Integer
--      |_  params = Object
function shcc.router.openOverlay( path, options )
    options = options or {};
    options.modal  = options.modal or shcc.router.DEFAULT_OVERLAY_MODAL;
    options.effect  = options.effect or shcc.router.DEFAULT_OVERLAY_OPEN_EFFECT;
    options.time    = options.time or shcc.router.DEFAULT_OVERLAY_EFFECT_TIME;
    options.params  = {
        overlay = true,
        data = options.data or {}
    };

    Global:dispatchEvent({
        name = Constants.Events.OVERLAY_OPENED
    });

    -- Track page if GoogleAnalytics is present
    if GoogleAnalytics then
        GoogleAnalytics:trackPage( path, klm.layover.trackingManager:getPageTitle( path ) );
    end


    Composer.showOverlay( path, options );

    shcc.router.overlayOpen = true;
end

-- Method to close the current overlay
-- @String: Effect to hide the overlay, defaults to shcc.router.DEFAULT_OVERLAY_CLOSE_EFFECT (optional)
-- @Integer: Time it takes to play the effect, defaults to shcc.router.DEFAULT_OVERLAY_EFFECT_TIME (optional)
function shcc.router.closeOverlay( effect, time )
    Composer.hideOverlay( effect or shcc.router.DEFAULT_OVERLAY_CLOSE_EFFECT, time or shcc.router.DEFAULT_OVERLAY_EFFECT_TIME );
    shcc.router.overlayOpen = false;

    Global:dispatchEvent({
        name = Constants.Events.OVERLAY_CLOSED
    });
end


-- Method to preload a scene
-- @String: Path to scene file (required)
function shcc.router.load( path )
    Composer.loadScene( path );
end

-- Method to remove a scene
-- @String: Path to scene file (required)
function shcc.router.remove( path )
    Composer.removeScene( path );
end

-- Method to preload a scene
-- @String: Path to scene file (required)
-- Returns the specified scene object, as returned from Composer:newScene(). Returns nil if the scene does not exist.
function shcc.router.get( path )
    return Composer.getScene( path );
end

function shcc.router.getCurrentScene()
    return Composer.getScene( shcc.router.getCurrentSceneName() );
end

-- Method to remove the previous scene
-- @String: Path to scene file (required)
function shcc.router.removePreviousScene()
    if shcc.router.getPreviousSceneName() then
        Composer.removeScene( shcc.router.getPreviousSceneName() );
    end
end

function shcc.router.removeHiddenScene()
    Composer.removeHidden();
end

-- Method to get the current scene path
function shcc.router.getCurrentSceneName()
    return Composer.getSceneName( 'current' );
end

-- Method to get the previous scene path
function shcc.router.getPreviousSceneName()
    -- Log.error( shcc.router.history[ #shcc.router.history-1 ], shcc.router.getHistoryLength() );
    return shcc.router.history[ #shcc.router.history-1 ];
end

-- Method to get the current overlay scene path
function shcc.router.getOverlayName()
    return Composer.getSceneName( 'overlay' );
end

-- Method to get the length of the history object
function shcc.router.getHistoryLength()
    return #shcc.router.history;
end

--
function shcc.router.getTransitionState()
    return shcc.router.transitionState;
end

function shcc.router.setVariable( var, value )
    Composer.setVariable( var, value );
end

function shcc.router.getVariable( var )
    return Composer.getVariable( var );
end

function shcc.router.removeFromHistory( startIndex, endIndex )
    for index = startIndex, endIndex do
        shcc.router.history[ index ] = nil
    end
end

--
-- Scene Queue
--
function shcc.router.addSceneToQueue( path, options )
    shcc.router.sceneQueue[ #shcc.router.sceneQueue+1 ] = {
        path = path,
        options = options
    };
end

function shcc.router.removeSceneFromQueue()
    shcc.router.sceneQueue[ 1 ] = nil;
end

function shcc.router.getNextInSceneQueue()
    return shcc.router.sceneQueue[ 1 ] or nil;
end

function shcc.router.navigateToNextSceneInQueue()
    shcc.router.isTransitioning = false;
    local nextScene = shcc.router.getNextInSceneQueue();
    if nextScene then
        _queueTransitionTimer = timer.performWithDelay( nextScene.options.time > 500 and 100 or 600, function()
            shcc.router.navigateTo( nextScene.path, nextScene.options );
            shcc.router.removeSceneFromQueue();
        end);
    end
end

--
--  Helpers
--

-- Method to set debugging on or off
function shcc.router.debug( value )
    Composer.isDebug = value;
end
