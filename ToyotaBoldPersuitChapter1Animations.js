/*
 *  [ Backbone - Javascript ]
 *  Project: Toyota - the Bold persuit
 *  url: http://www.theboldpursuit.ae/
 *
 *  Dit project was een grote interactive comic met veel animaties (on scroll).
 *  Tijdens dit project was ik verantwoordelijk voor de comic experience. In deze file staan alle animaties van het eerste chapter.
 *  Deze animaties moesten op elk scherm formaat (incl. mobile) goed werken en kloppen qua positionering.
 *  Vandaar dat overal gebruik wordt gemaakt van xPercent en yPercent.
 *  
 */

define([
    'superhero',
    'shared/models/UserModel',
    'shared/views/pages/ExperienceBaseView',
    'shared/utils/SoundManager'
], function(Superhero, UserModel, ExperienceBaseView, SoundManager) {

    return ExperienceBaseView.extend({

        className: 'page page--experience',
        template: 'pages/experience-chapter01',

        ui: {
            headers: 'h1',

            // Frame 01
            frame_01__bg_top: '.section1__bg_top',
            frame_01__bg_bottom: '.section1__bg_bottom',
            frame_01__finger: '.section1__finger',

            // Frame 02
            frame_02__pointer_fx: '.section1__pointer-fx',
            frame_02__pointer: '.section1__pointer',

            // Frame 03
            frame_03__track1_T: '.section1__track1_T',
            frame_03__track1_R: '.section1__track1_R',
            frame_03__track1_A: '.section1__track1_A',
            frame_03__track1_C: '.section1__track1_C',
            frame_03__track1_K: '.section1__track1_K',
            frame_03__track1_1: '.section1__track1_1',

            frame_03__equalizer: '.section1__equalizer',
            frame_03__bars: '.section1__equalizer-bar',

            frame_03__equalizer_row1: '.section1__equalizer-row1',
            frame_03__equalizer_row1_bar1: '.section1__equalizer-row1-bar1',
            frame_03__equalizer_row1_bar2: '.section1__equalizer-row1-bar2',
            frame_03__equalizer_row1_bar3: '.section1__equalizer-row1-bar3',
            frame_03__equalizer_row1_bar4: '.section1__equalizer-row1-bar4',
            frame_03__equalizer_row1_bar5: '.section1__equalizer-row1-bar5',
            frame_03__equalizer_row1_bar6: '.section1__equalizer-row1-bar6',

            frame_03__equalizer_row2: '.section1__equalizer-row2',
            frame_03__equalizer_row2_bar1: '.section1__equalizer-row2-bar1',
            frame_03__equalizer_row2_bar2: '.section1__equalizer-row2-bar2',
            frame_03__equalizer_row2_bar3: '.section1__equalizer-row2-bar3',
            frame_03__equalizer_row2_bar4: '.section1__equalizer-row2-bar4',
            frame_03__equalizer_row2_bar5: '.section1__equalizer-row2-bar5',
            frame_03__equalizer_row2_bar6: '.section1__equalizer-row2-bar6',

            frame_03__equalizer_row3: '.section1__equalizer-row3',
            frame_03__equalizer_row3_bar1: '.section1__equalizer-row3-bar1',
            frame_03__equalizer_row3_bar2: '.section1__equalizer-row3-bar2',
            frame_03__equalizer_row3_bar3: '.section1__equalizer-row3-bar3',
            frame_03__equalizer_row3_bar4: '.section1__equalizer-row3-bar4',
            frame_03__equalizer_row3_bar5: '.section1__equalizer-row3-bar5',
            frame_03__equalizer_row3_bar6: '.section1__equalizer-row3-bar6',

            frame_03__equalizer_row4: '.section1__equalizer-row4',
            frame_03__equalizer_row4_bar1: '.section1__equalizer-row4-bar1',
            frame_03__equalizer_row4_bar2: '.section1__equalizer-row4-bar2',
            frame_03__equalizer_row4_bar3: '.section1__equalizer-row4-bar3',
            frame_03__equalizer_row4_bar4: '.section1__equalizer-row4-bar4',
            frame_03__equalizer_row4_bar5: '.section1__equalizer-row4-bar5',
            frame_03__equalizer_row4_bar6: '.section1__equalizer-row4-bar6',

            frame_03__equalizer_row5: '.section1__equalizer-row5',
            frame_03__equalizer_row5_bar1: '.section1__equalizer-row5-bar1',
            frame_03__equalizer_row5_bar2: '.section1__equalizer-row5-bar2',
            frame_03__equalizer_row5_bar3: '.section1__equalizer-row5-bar3',
            frame_03__equalizer_row5_bar4: '.section1__equalizer-row5-bar4',
            frame_03__equalizer_row5_bar5: '.section1__equalizer-row5-bar5',
            frame_03__equalizer_row5_bar6: '.section1__equalizer-row5-bar6',

            // Frame 04
            frame_04__car_icon: '.section1__car-icon',

            // Frame 05
            frame_05__bg: '.section2__bg',
            frame_05__arm: '.section2__arm',
            frame_05__hand: '.section2__hand',
            frame_05__phone: '.section2__phone',
            frame_05__text: '.section2__text',
            frame_05__buzz1: '.section2__buzz1',
            frame_05__buzz2: '.section2__buzz2',
            frame_05__buzz3: '.section2__buzz3',

            // Frame 06
            frame_06__bg: '.section2__bg2',
            frame_06__phone_big: '.section2__phone-big',
            frame_06__profile_picture_wrapper: '.section2__profile-picture-wrapper',
            frame_06__profile_picture_mask: '.section2__profile-picture-mask',
            frame_06__profile_picture: '.section2__profile-picture',
            frame_06__profile_name: '.section2__profile-name',
            frame_06__text_message: '.section2__text-message',

            // Frame 07
            frame_07__bg: '.section3__bg',
            frame_07__biker1: '.section3__biker1',
            frame_07__biker2: '.section3__biker2',
            frame_07__biker1_light: '.section3__biker1-light',
            frame_07__biker2_light: '.section3__biker2-light',

            // Frame 08
            frame_08__left_eye: '.section4__left-eye',
            frame_08__right_eye: '.section4__right-eye',
            frame_08__bg: '.section4__bg',
            frame_08__face_flicker: '.section4__face-flicker',
            frame_08__face_mask: '.section4__face-mask',
            frame_08__window_shines: '.section4__window-shines',

            choice_hide: '.experience-choice--hide',
            choice_drive: '.experience-choice--drive',
        },

        events: {
            'click .experience-choice--hide': '_choiceHideClickedHandler',
            'click .experience-choice--drive': '_choiceDriveClickedHandler',
        },

        _wiggleArray: [],
        _wiggleInterval: null,
        _wiggleIntervalCount: 0,
        _wiggleMaxIntervalCount: 3,

        _isEqualizerActive: false,
        _equalizerInterval: null,

        initialize: function() {
            ExperienceBaseView.prototype.initialize.apply(this);
        },

        onInitialized: function() {
            _.bindAll( this, '_startWiggleObject', '_toggleEqualizerAnimation', '_animateEqualizer' );

            this.addOutroVideoComponent();

            ExperienceBaseView.prototype.onInitialized.apply(this);

            this._setUserData();
            this._setupTimelines();

            this._createTransitionInAnimation();
        },

        immediateTransitionIn: function() {
            this._transitionInAnimation.play();
        },

        _createTransitionInAnimation: function() {
            this._transitionInAnimation = new TimelineMax({paused:true, onComplete:this.components.introVideo.start});
            this._transitionInAnimation.insert(new TweenMax.fromTo(this.el, 1, {y:'100%'}, {y:'0%', force3D:'auto', ease:Power3.easeOut}), 0);
        },

        _setUserData: function() {
            if (UserModel.get('name')) this.ui.frame_06__profile_name.textContent = UserModel.get('name').split(' ')[0];
            if (UserModel.get('picture')) {
                console.log( 'picture Data' );
                this.ui.frame_06__profile_picture.src = UserModel.get('picture');
            } else if (UserModel.get('picture_fb_url')) {
                console.log( 'picture fb url' );
                this.ui.frame_06__profile_picture.src = UserModel.get('picture_fb_url');
            }
        },

        _setupTimelines: function() {
            /*
             *
             *  Setup sounds 
             * 
             */
            var sounds = {
                tvc_to_ill_trans: SoundManager.getSound('sound/tvc_to_ill_trans'),
                
                ch1_engine_on: SoundManager.getSound('sound/ch1_engine_on'),
                ch1_SMS: SoundManager.getSound('sound/ch1_SMS'),
                ch1_message: SoundManager.getSound('sound/ch1_message'),
                ch1_bikerev_loop: SoundManager.getSound('sound/ch1_bikerev_loop'),
                ch1_eerie: SoundManager.getSound('sound/ch1_eerie'),

                action: SoundManager.getSound('sound/action'),
            };

            /*
             *
             *  TVC TO ILLUSTRATION
             * 
             */
            TweenMax.delayedCall( 3.7, sounds.tvc_to_ill_trans.play );

            /*
             *
             *  FRAME 01 t/m 04
             *  TIMELINE 1
             *
             */
            
            TweenMax.set( this.ui.frame_01__bg_bottom, {x: 0, force3D: true});
            TweenMax.set( this.ui.frame_03__equalizer, {alpha: 0});

            this.timeline1 = new TimelineMax({paused:true, useFrames:true});
    
            /*
             *  FINGER
             */
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_01__finger, 10, 
                {xPercent: 0, yPercent: 0, force3D: true},
                {xPercent: -30, yPercent: 30}
            ), 10);
            
            /*
             *  POINTER & POINTER FX
             */
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_02__pointer_fx, 10, 
                {alpha: 0, xPercent: -20, yPercent: -30, rotation: -60, transformOrigin: 'right', force3D: true},
                {alpha: 1, xPercent: 0, yPercent: 0, rotation: 0, ease: Quad.easeOut}
            ), 5);
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_02__pointer, 10, 
                {rotation: 0, transformOrigin: 'right', force3D: true},
                {rotation: 60, ease: Quad.easeOut}
            ), 5);


            /*
             *  TRACK 1
             */
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_03__track1_T, 0.1,
                {alpha: 0, force3D: true},
                {alpha: 1}
            ), 15);
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_03__track1_R, 0.1,
                {alpha: 0, force3D: true},
                {alpha: 1}
            ), 16);
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_03__track1_A, 0.1,
                {alpha: 0, force3D: true},
                {alpha: 1}
            ), 17);
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_03__track1_C, 0.1,
                {alpha: 0, force3D: true},
                {alpha: 1}
            ), 18);
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_03__track1_K, 0.1,
                {alpha: 0, force3D: true},
                {alpha: 1}
            ), 19);
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_03__track1_1, 0.1,
                {alpha: 0, force3D: true},
                {alpha: 1}
            ), 20);
            

            /*
             *  CAR ICON
             */
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_04__car_icon, 10,
                {rotation: 0, xPercent: 0, yPercent: 0, force3D: true},
                {rotation: 0, xPercent: 90, yPercent: -90, ease: Linear.easeNone}
            ), 10);
            this.timeline1.insert( TweenMax.fromTo( this.ui.frame_04__car_icon, 10,
                {rotation: 0, xPercent: 90, yPercent: -90, force3D: true},
                {rotation: -50, xPercent: 140, yPercent: -210, ease: Linear.easeNone}
            ), 20);

            /*
             *  EQUALIZER ANIMATION
             */
            this.timeline1.addCallback( this._toggleEqualizerAnimation, 20 );
            this.timeline1.addCallback( this._toggleEqualizerAnimation, this.timeline1.duration()-0.1 );

            /*
             *  SOUNDS
             */
            this.timeline1.insert( TweenMax.to( sounds.ch1_engine_on, 2, {autoVolume: 1}), 0.1 );



            /*
             *
             *  FRAME 05 & 06
             *  TIMELINE 2
             *
             */
            
            TweenMax.set( this.ui.frame_05__buzz1, {alpha: 0} );
            TweenMax.set( this.ui.frame_05__buzz2, {alpha: 0} );
            TweenMax.set( this.ui.frame_05__buzz3, {alpha: 0} );

            this.timeline2 = new TimelineMax({paused:true, useFrames:true});

            /*
             *  BACKGROUND
             */
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_05__bg, 10,
                {alpha: 0, force3D: true},
                {alpha: 1}
            ), 0);

            /*
             *  TEXT
             */
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_05__text, 2, 
                {alpha: 0, force3D: true},
                {alpha: 1}
            ), 12);
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_05__text, 8,
                {yPercent: 30, force3D: true},
                {yPercent: 0, ease: Quad.easeInOut}
            ), 12);

            /*
             *  ARM
             */
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_05__arm, 10,
                {scaleX: 0.8, scaleY: 0.9, xPercent: -5, force3D: true},
                {scaleX: 1, scaleY: 1, xPercent: 0, ease: Quad.easeInOut}
            ), 10);

            /*
             *  HAND
             */
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_05__hand, 10,
                {scale: 0.8, xPercent: -25, rotation: -15, transformOrigin: '20px 130px', force3D: true},
                {scale: 1, xPercent: 0, rotation: 0, ease: Quad.easeInOut}
            ), 10);

            /*
             *  PHONE
             */
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_05__phone, 20,
                {rotation: -15, transformOrigin: 'center', force3D: true},
                {rotation: 0, ease: RoughEase.ease}
            ), 10);

            /*
             *  RIGHT FRAME
             */
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_06__bg, 7,
                {xPercent: 10, yPercent: 100, force3D: true},
                {xPercent: 0, yPercent: 0, ease: Quad.easeOut}
            ), 19);

            /*
             *  BIG PHONE
             */
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_06__phone_big, 7,
                {xPercent: 20, yPercent: 120, force3D: true},
                {xPercent: 0, yPercent: 0, ease: Quad.easeOut}
            ), 19);

            /*
             *  PROFILE NAME
             */
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_06__profile_name, 1,
                {alpha: 0, xPercent: -50, yPercent: -45, force3D: true},
                {alpha: 1, xPercent: 0, yPercent: 0, ease: Quad.easeOut}
            ), 26);

            /*
             *  PROFILE PICTURE
             */
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_06__profile_picture_wrapper, 1,
                {alpha: 0, xPercent: -15, yPercent: 30, force3D: true},
                {alpha: 1, xPercent: 0, yPercent: 0, ease: Quad.easeOut}
            ), 27);

            /*
             *  TEXT MESSAGE
             */
            this.timeline2.insert( TweenMax.fromTo( this.ui.frame_06__text_message, 1,
                {alpha: 0, xPercent: -18, yPercent: 30, force3D: true},
                {alpha: 1, xPercent: 0, yPercent: 0, ease: Quad.easeOut}
            ), 26);

            this.timeline2.to( this.ui.frame_06__bg, 12, {x: 0}, 26);

            /*
             *  WIGGLE
             */
            this.timeline2.addCallback( this._toggleWiggleObject, 15, [ 2, [this.ui.frame_05__buzz1, this.ui.frame_05__buzz2, this.ui.frame_05__buzz3], 5, 1, 'center'], this);
            this.timeline2.addCallback( this._toggleWiggleObject, this.timeline2.duration()-0.1, [ 2, [this.ui.frame_05__buzz1, this.ui.frame_05__buzz2, this.ui.frame_05__buzz3], 5, 1, 'center'], this);

            /*
             *  SOUNDS
             */
            this.timeline2.insert( TweenMax.to( sounds.ch1_SMS, 1, {autoVolume: 1}), 10 );
            this.timeline2.insert( TweenMax.to( sounds.ch1_message, 1, {autoVolume: 1}), 26 );


            /*
             *
             *  FRAME 07
             *  TIMELINE 3
             *
             */

            this.timeline3 = new TimelineMax({paused: true, useFrames: true});
            /*
             *  BIKER 1
             */
            this.timeline3.insert( TweenMax.fromTo( this.ui.frame_07__biker1, 10, 
                {scale: 0.5, yPercent: 28, force3D: true},
                {scale: 1, yPercent: 0, ease: Quad.easeIn}
            ), 0);
            this.timeline3.insert( TweenMax.fromTo( this.ui.frame_07__biker1_light, 10, 
                {alpha: 0.4, scale: 0.5, yPercent: 28, force3D: true},
                {alpha: 1, scale: 1, yPercent: 0, ease: Quad.easeIn}
            ), 0);

            /*
             *  BIKER 2
             */
            this.timeline3.insert( TweenMax.fromTo( this.ui.frame_07__biker2, 10, 
                {scale: 0.7, xPercent: -10, yPercent: 10, force3D: true},
                {scale: 1.2, xPercent: 0, yPercent: 0, ease: Quad.easeIn}
            ), 0);
            this.timeline3.insert( TweenMax.fromTo( this.ui.frame_07__biker2_light, 10, 
                {alpha: 0.7, scale: 0.4, xPercent: -10, yPercent: 10, force3D: true},
                {alpha: 1.2, scale: 1, xPercent: 0, yPercent: 0, ease: Quad.easeIn}
            ), 0);

            /*
             *  SOUNDS
             */
            this.timeline3.insert( TweenMax.fromTo( sounds.ch1_bikerev_loop, 3, {autoVolume: 0}, {autoVolume: 1}), 0 );
            this.timeline3.addCallback( SoundManager.playSound, 1, ['sound/ch1_bikerev_loop', 0, 1, -1], SoundManager );
            this.timeline3.insert( TweenMax.to( sounds.ch1_bikerev_loop, 1, {autoVolume: 0}), 7 );


            /*
             *
             *  FRAME 08
             *  TIMELINE 4
             *
             */

            this.timeline4 = new TimelineMax({paused: true, useFrames: true});
            /*
             *  EYES
             */
            this.timeline4.insert( TweenMax.fromTo( this.ui.frame_08__left_eye, 6, 
                {xPercent: 30, force3D: true},
                {xPercent: 0, ease: Quad.easeInOut}
            ), 0);
            this.timeline4.insert( TweenMax.fromTo( this.ui.frame_08__right_eye, 6, 
                {xPercent: 30, force3D: true},
                {xPercent: 0, ease: Quad.easeInOut}
            ), 0);
            
            /*
             *  BACKGROUND
             */
            this.timeline4.insert( TweenMax.fromTo( this.ui.frame_08__bg, 6, 
                {xPercent: 3, force3D: true},
                {xPercent: 0, ease: Quad.easeInOut}
            ), 0);
            this.timeline4.insert( TweenMax.fromTo( this.ui.frame_08__face_mask, 6, 
                {xPercent: 23, force3D: true},
                {xPercent: 0, ease: Quad.easeInOut}
            ), 0);

            /*
             *  FACE FLICKER
             */
            this.timeline4.insert( TweenMax.fromTo( this.ui.frame_08__face_flicker, 6, 
                {alpha: 0.6, xPercent: 8, yPercent: 100, force3D: true},
                {alpha: 0.8, xPercent: -5, yPercent: -100, ease: Quad.easeInOut}
            ), 0);

            /*
             *  WINDOW SHINES
             */
            this.timeline4.insert( TweenMax.fromTo( this.ui.frame_08__window_shines, 10,
                {alpha: 1, xPercent: -100, force3D: true},
                {alpha: 0, xPercent: 300, ease: Quad.easeInOut}
            ), 0);

            /*
             *  SOUNDS
             */
            this.timeline4.insert( TweenMax.fromTo( sounds.ch1_eerie, 1, {autoVolume: 0}, {autoVolume: 1}), 4 );
            this.timeline4.addCallback( sounds.tvc_to_ill_trans.play, this.timeline4.duration()-0.1 );

            
            /*
             *
             *  OUTRO
             *  
             *
             */

            this.outro = new TimelineMax({paused: true});
            this.outro.add([
                /*
                 *  WHITE INK
                 */
                TweenMax.staggerFromTo([this.ui.choice_hide, this.ui.choice_drive], 0.3,
                    {alpha: 0, x: -200},
                    {alpha: 1, x:0, ease: Back.easeOut},
                0.2),

               
            ]);

        },

        _videoOutroEnded: function() {
            this.outro.play();
        },

        _toggleWiggleObject: function( id, objects, rotation, interval, origin ) {
            if (this._wiggleArray[ id ]) {
                this._wiggleArray[ id ] = null;
                if (this._wiggleInterval) clearInterval( this._wiggleInterval );
                this._stopWiggleObjects( objects );
            } else {
                this._wiggleArray[ id ] = objects;
                if (interval > 0) {
                    this._wiggleIntervalCount = 0;
                    this._startWiggleObject( objects, rotation, 3, origin );
                    this._wiggleInterval = setInterval( this._startWiggleObject, interval*1000, objects, rotation, 3, origin );
                } else {
                    this._startWiggleObject( objects, rotation, -1, origin );
                }
            }
        },

        _startWiggleObject: function( objects, rotation, repeat, origin ) {
            TweenMax.killTweensOf( objects );
            rotation = rotation || 2;
            origin = origin || 'right bottom';
            repeat = repeat || -1;

            for (var i = 0, limit = objects.length; i < limit; i++) {
                TweenMax.to( objects[ i ], 0.1, {delay: 0.2*i, alpha: 1});
                TweenMax.fromTo( objects[ i ], .1,
                    {
                        rotation: 0
                    }, {
                        delay: 0.2*i,
                        rotation: rotation,
                        yoyo: true,
                        repeat: repeat,
                        transformOrigin: origin,
                        ease: RoughEase.ease
                    }
                );
            }

            if (this._wiggleIntervalCount >= this._wiggleMaxIntervalCount) {
                if (this._wiggleInterval) clearInterval( this._wiggleInterval );
            } else {
                this._wiggleIntervalCount = this._wiggleIntervalCount+1;
            }
        },

        _stopWiggleObjects: function( objects ) {
            TweenMax.killTweensOf( objects );
            for (var i = 0, limit = objects.length; i < limit; i++) {
                TweenMax.to( objects[ i ], 0.1, {alpha: 0});
            }
        },

        _toggleEqualizerAnimation: function() {
            console.log( 'active', this._isEqualizerActive );
            if (this._isEqualizerActive) {
                this._endEqualizerAnimation();
            } else {
                this._startEqualizerAnimation();
            }
        },

        _startEqualizerAnimation: function() {
            this._isEqualizerActive = true;
            console.log( '_startEqualizerAnimation' );

            // Fade in equalizer
            TweenMax.to( this.ui.frame_03__equalizer, 0.3, {
                alpha: 1
            });

            // Randomize equalizer rows
            for (var i = 0, limit = this.ui.frame_03__bars.length; i < limit; i++) {
                TweenMax.set( this.ui.frame_03__bars[i], {alpha: 0});
            }

            if (this._equalizerInterval) clearInterval( this._equalizerInterval );
            this._equalizerInterval = setInterval( this._animateEqualizer, 200 );
        },

        _endEqualizerAnimation: function() {
            this._isEqualizerActive = false;
            console.log( '_endEqualizerAnimation' );
            
            if (this._equalizerInterval) clearInterval( this._equalizerInterval );

            // Fade out equalizer
            TweenMax.to( this.ui.frame_03__equalizer, 0.3, {
                alpha: 0
            });
        },

        _animateEqualizer: function() {
            this._animateInEqualizerRow( this.ui.frame_03__equalizer_row1.children, 0 );
            this._animateInEqualizerRow( this.ui.frame_03__equalizer_row2.children, 0.01 );
            this._animateInEqualizerRow( this.ui.frame_03__equalizer_row3.children, 0.02 );
            this._animateInEqualizerRow( this.ui.frame_03__equalizer_row4.children, 0.03 );
            this._animateInEqualizerRow( this.ui.frame_03__equalizer_row5.children, 0.04 );
        },

        _animateInEqualizerRow: function( objects, delay ) {
            var maxItems = Math.floor(Math.random()*objects.length);
            if (maxItems <= 1) maxItems = 2;
            for (var i = 0, limit = objects.length; i < limit; i++) {
                if (i <= maxItems) {
                    TweenMax.set( objects[ i ], {alpha: 1});
                } else {
                    TweenMax.to( objects[ i ], 0.2, {delay: (0.05*i)-(0.05*(i-1)), alpha: 0});
                }
            }
        },

        _doUpdateTimelines: function() { //This gets called from ExperienceBaseView
            this.timeline1.progress(this._getSectionProgressByIndex(1));
            this.timeline2.progress(this._getSectionProgressByIndex(2));
            this.timeline3.progress(this._getSectionProgressByIndex(3));
            this.timeline4.progress(this._getSectionProgressByIndex(4));
        },

        _choiceHideClickedHandler: function(e) {
            e.preventDefault();

            UserModel.set('selection_1', 1);
            Superhero.history.navigate('experience/hide', true);
        },

        _choiceDriveClickedHandler: function(e) {
            e.preventDefault();

            UserModel.set('selection_1', 2);
            Superhero.history.navigate('experience/drive', true);
        }

    });

});