--[[
 *  [ Corona SDK - Lua ]
 *  Project: Erecode
 *
 *  Omdat deze app voor kinderen bestemd was moesten we de app zo leuk mogelijk animeren, een van de belangrijkste animaties
 *  in deze app is de achtergrond. Hiervoor hadden we een transitie bedacht die het patroon en de kleur kan aanpassen in een snelle animatie.
 *  Deze file regelt de transitie van de huidige naar de nieuwe background met een masker. De data voor de background patterns en kleuren
 *  worden bijgehouden in een aparte data file zodat het geheel makkelijker te onderhouden is.
 */
]]

local M = {};

function M:new( packages )

    local   _view,
            _container,

            _libs, _data, _sheets,

            _backgroundColor,
            _backgroundPattern,
            _newBackgroundColor,
            _pattern,

            _repeatBackgroundTransition,
            _currentBackground;

    _view = shcc.Base_View:new( AppConstants.BACKGROUND_VIEW );
    _view.onHideComplete = function() end;
    _view.isVisible = false;

    _container = display.newGroup();
    _container.x = display.contentCenterX;
    _container.y = display.contentCenterY;
    _container.name = 'Side menu container';
    _view:insert( _container );

    _sheets = {};
    for k,v in pairs( packages.sheets ) do
        sheets[k] = SpriteGrabber:grabSheet( v.package, v.url );
    end

    _data = packages.data;
    _libs = packages.libs;

    -- Build view
    -- Set texturewrap to repeat X and Y
    display.setDefault( "textureWrapX", "repeat" );
    display.setDefault( "textureWrapY", "repeat" );

    _pattern = {
        type = 'image',
        filename = data.backgrounds.default.pattern
    };

    _backgroundColor = display.newRect( 0, 0, display.contentWidth, display.contentHeight );
    _backgroundColor:setFillColor( ColorUtil:convertHexToNumber( data.backgrounds.default.colors[2] ) );
    _container:insert( _backgroundColor );

    _backgroundPattern = display.newRect( 0, 0, display.contentWidth, display.contentHeight );
    _backgroundPattern.fill = _pattern;
    _backgroundPattern.fill.scaleX, _backgroundPattern.fill.scaleY = data.backgrounds.default.scaleX, data.backgrounds.default.scaleY;
    _backgroundPattern.fill.rotation = data.backgrounds.default.rotation;
    _backgroundPattern:setFillColor( ColorUtil:convertHexToNumber( data.backgrounds.default.colors[2] ) );
    _container:insert( _backgroundPattern );

    -- reset texturewrap to default
    display.setDefault( "textureWrapX", "clampToEdge" );
    display.setDefault( "textureWrapY", "clampToEdge" );

    -- Private methods
    function _repeatBackgroundTransition()
        tween.kill( _backgroundPattern.fill );
        if _backgroundPattern.fill.x >= 1 then
            _backgroundPattern.fill.x = 0;
            _backgroundPattern.fill.y = 0;
        end

        tween.to( _backgroundPattern.fill, 0.25, {
            x = _backgroundPattern.fill.x + 0.1,
            y = _backgroundPattern.fill.y + 0.1,
            onComplete = _repeatBackgroundTransition
        });
    end
    _repeatBackgroundTransition();


    --
    --  Private methods
    --
    function _setupEventListeners()
    end

    function _removeEventListeners()
    end


    --
    -- Public methods
    --

    -- _view:show()
    -- Method is called on mediator:onRegister()
    -- This method should only handle the show animation
    function _view:show()
        -- Set the view to visible
        self.isVisible = true;

        -- Fade in container
        tween.to( _container, 0.6, {
            alpha       = 1,
            transition  = easing.outExpo
        });

        _setupEventListeners();
    end


    -- _view:hide()
    -- Method is called when the mediator recieves an AppNotifications.VIEW_HIDE notification
    -- This method should only handle the hide animation
    function _view:hide()
        -- Cancel transitions
        tween.kill( _container );

        -- Fade out container
        tween.to( _container, 0.6, {
            alpha       = 0,
            transition  = easing.inExpo,
            onComplete  = _view.onHideComplete
        });

        _removeEventListeners();
    end

    -- _view:changeBackground()
    -- Method is called when the mediator recieves an AppNotifications.CHANGE_BACKGROUND notification
    -- This method handles the change of the background image/color
    function _view:changeBackground( screen )
        if not screen or screen == _currentBackground then
            return false;
        end
        _currentBackground = screen;

        local mask = graphics.newMask( 'assets/images/ui/chart/chart-mask.png' );
        local duration = 0.8;
        _pattern = {
            type = 'image',
            filename = data.backgrounds[ screen ].pattern
        };

        _newBackgroundColor = display.newRect( 0, 0, display.contentWidth, display.contentHeight );
        _newBackgroundColor:setFillColor( ColorUtil:convertHexToNumber( data.backgrounds[ screen ].colors[2] ) );
        _newBackgroundColor:setMask( mask );
        _newBackgroundColor.maskScaleX, _newBackgroundColor.maskScaleY = 0.1, 0.1;

        display.setDefault( "textureWrapX", "repeat" );
        display.setDefault( "textureWrapY", "repeat" );

        _backgroundPattern.fill = _pattern;
        _backgroundPattern.fill.scaleX, _backgroundPattern.fill.scaleY = data.backgrounds[ screen ].scaleX, data.backgrounds[ screen ].scaleY;
        _backgroundPattern.fill.rotation = data.backgrounds[ screen ].rotation;

        display.setDefault( "textureWrapX", "clampToEdge" );
        display.setDefault( "textureWrapY", "clampToEdge" );


        _backgroundPattern:setFillColor( ColorUtil:convertHexToNumber( data.backgrounds[ screen ].colors[1] ) );
        _backgroundPattern:setMask( mask );
        _backgroundPattern.maskScaleX, _backgroundPattern.maskScaleY = 0.1, 0.1;

        _container:insert( _newBackgroundColor );
        _container:insert( _backgroundPattern );
        
        tween.to( _backgroundPattern, duration, {
            maskScaleX = 2,
            maskScaleY = 2,
            transition = easing.inOutQuad
        });
        
        tween.to( _newBackgroundColor, duration, {
            delay = 100,
            maskScaleX = 2,
            maskScaleY = 2,
            transition = easing.inOutQuad,
            onComplete = function()
                _container:remove( _newBackgroundColor );
                _newBackgroundColor = nil;
                _backgroundColor:setFillColor( ColorUtil:convertHexToNumber( data.backgrounds[ screen ].colors[2] ) );
            end
        });

    end

    -- _view:dispose()
    -- Method is called on mediator:onRemove()
    -- This method should always call the superDispose()
    -- Make sure to set all functions and variables to nil
    local superDispose = _view['dispose'];
    function _view:dispose()
        tween.killAll();

        superDispose();
    end

    return _view;
end

return M;