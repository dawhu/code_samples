/*
 *  [ Backbone - Javascript ]
 *  Project: Personal portfolio
 *  url: http://wouter-versluys.nl
 *
 *  Voor mijn eigen portfolio heb ik met behulp van ThreeJS een background pattern gemaakt dat gebruikt wordt om meer diepte te creëren.
 *  Doormiddel van application wide events kan de background geanimeerd worden om zo pagina transities ook duidelijker te maken.
 *  
 */

define([
    'extendedBackbone',
    'threejs',

    'utils/ResizeManager',
    'utils/ScrollManager'
], function( extendedBackbone, THREE, ResizeManager, ScrollManager ) {

    return extendedBackbone.Component.extend({

        EVENTS: {
            SET_SPEED: 'backgroundComponent::setSpeed',
            MOVE: 'backgroundComponent::move',
            PAUSE: 'backgroundComponent::pause',
            RESUME: 'backgroundComponent::resume'
        },

        onInitialized: function() {

            _.bindAll( this, '_backgroundSetSpeedHandler', '_moveBackgroundCompleteHandler', '_render', '_mouseMoveHandler', '_backgroundMoveHandler', '_backgroundPauseHandler', '_backgroundResumeHandler');

            // Different background settings for easy testing purposes
            this._backgroundSettings = {
                white: {
                    // Size and segments
                    size: {
                        width: ResizeManager.viewportWidth()*1.4,
                        height: ResizeManager.viewportHeight()*4
                    },
                    segments: {
                        // x: 10,
                        // y: 10
                    },

                    // Material
                    materialType: THREE.MeshPhongMaterial,
                    material: {
                        shading: THREE.FlatShading,
                        vertexColors: THREE.FaceColors,
                        color: 0xffffff,
                        specular: 0xffffff,
                        emissive: 0x666666,
                        wireframe: false
                    },

                    // Lights
                    lights: {
                        ambient: {
                            color: 0x666666
                        },
                        directional: {
                            color: 0xdfebff,
                            intensity: {
                                start: 0.2,
                                end: 0.2
                            },
                            size: 500,
                            x: 0,
                            y: 20,
                            z: 200
                        }
                    }
                },

                black: {
                    // Size and segments
                    size: {
                        width: ResizeManager.viewportWidth()*1.2,
                        height: ResizeManager.viewportHeight()*1.2
                    },
                    segments: {
                        // x: 20
                    },

                    // Material
                    materialType: THREE.MeshStandardMaterial,
                    material: {
                        shading: THREE.FlatShading,
                        vertexColors: THREE.FaceColors,
                        metalness: 0.4,
                        color: 0x101010,
                        // specular: 0xffffff,
                        emissive: 0x000000,
                        wireframe: false
                    },

                    // Lights
                    lights: {
                        ambient: {
                            color: 0x222222
                        },
                        directional: {
                            color: 0xdfebff,
                            intensity: {
                                start: 1,
                                end: 1
                            },
                            size: 500,
                            x: 0,
                            y: 20,
                            z: 200
                        }
                    }
                },

                red: {
                    // Size and segments
                    size: {
                        width: ResizeManager.viewportWidth()*1.4,
                        height: ResizeManager.viewportHeight()*6
                    },
                    segments: {
                        // x: 6
                    },

                    // Material
                    materialType: THREE.MeshStandardMaterial,
                    material: {
                        shading: THREE.FlatShading,
                        vertexColors: THREE.FaceColors,
                        metalness: 0.2,
                        color: 0xff0000,
                        // specular: 0xffffff,
                        emissive: 0x222222,
                        wireframe: false
                    },

                    // Lights
                    lights: {
                        ambient: {
                            color: 0x666666
                        },
                        directional: {
                            color: 0xdfebff,
                            intensity: {
                                start: 0.2,
                                end: 0.2
                            },
                            size: 500,
                            x: 0,
                            y: 20,
                            z: 200
                        }
                    }
                }
            }
            
            // Set the background you want to use
            this._activeSettings = this._backgroundSettings.white;

            // Setup canvas and renderer
            this._setupScene();

            // Sets the base background speed
            this._backgroundSpeed = 0.5;

            // Create 4 instances of the background
            // used for creating the infinite background
            this._createBackground();
            this._createBackground();
            this._createBackground();
            this._createBackground();

            this._createLights();

            this._setupEventlisteners();

            this._scroll();

        },

        _setupEventlisteners: function() {
            
            // Enable the tweenmax ticker to render the scene
            TweenMax.ticker.addEventListener('tick', this._render);

            // Scroll event listener
            this.listenTo( ScrollManager, ScrollManager.EVENTS.SCROLL, this._scrollHandler );
            
            // Mouse move check within the window
            window.addEventListener('mousemove', this._mouseMoveHandler );

            // Router
            this.listenTo( App.router, App.router.EVENTS.ROUTE_START, this._routeStartHandler );

            // Background specific listeners
            App.events.on( this.EVENTS.SET_SPEED, this._backgroundSetSpeedHandler );
            App.events.on( this.EVENTS.MOVE, this._backgroundMoveHandler );
            App.events.on( this.EVENTS.PAUSE, this._backgroundPauseHandler );
            App.events.on( this.EVENTS.RESUME, this._backgroundResumeHandler );

        },

        _setupScene: function() {

            // Set basic properties for the scene
            var width = ResizeManager.viewportWidth();
            var height = ResizeManager.viewportHeight();
            var camera_angle = 45;
            var aspect = width/height;

            // Create the renderer instance and set the basics
            this._renderer = new THREE.WebGLRenderer({alpha:true});
            this._renderer.setSize( width, height );
            this._renderer.setClearColor( 0xffffff, 0 );

            // Enable shadows
            this._renderer.shadowMap.enabled = true;
            this._renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
            this._renderer.shadowMapSoft = true;

            // Add canvas element
            this.el.appendChild( this._renderer.domElement );

            // Set renderer with and height
            this._renderer.domElement.style.width = '100%';
            this._renderer.domElement.style.height = '100%';

            // Create an orthographic camera
            this._camera = new THREE.OrthographicCamera( width/-2, width/2, height/2, height/-2, 1, 1000 );
            this._startingCameraPosition = {
                x: 0,
                y: 0,
                z: 500
            }
            this._camera.position.x = this._startingCameraPosition.x;
            this._camera.position.y = this._startingCameraPosition.y;
            this._camera.position.z = this._startingCameraPosition.z;

            // Set camera direction to scene center
            this._camera.lookAt({
                x: 0,
                y: 0,
                z: 0
            });

            this._scene = new THREE.Scene();
            this._scene.add( this._camera );

        },

        _createBackground: function( axis ) {

            var width = this._activeSettings.size.width;
            var height = this._activeSettings.size.height;

            // Calculate the segments needed for screenwidth/height (if no default is set).
            var planeHeight = 4;
            var segmentsX = this._activeSettings.segments.x || Math.max(Math.ceil(width/300), 3);
            var segmentsY = this._activeSettings.segments.y || Math.max(Math.floor(segmentsX*(height / width)), 2);
            this._segments = {
                x: segmentsX,
                y: segmentsY
            };

            // Create background geometry and material
            var backgroundGeometry = new THREE.PlaneGeometry( width, height, segmentsX, segmentsY );
            var backgroundMaterial = new this._activeSettings.materialType(this._activeSettings.material);

            // Create mesh from geometry and material
            // Enable shadows (needed to show the different polygons)
            var background = new THREE.Mesh(backgroundGeometry, backgroundMaterial);
            background.receiveShadow = true;
            background.castShadow = true;
            
            // Push the new background to the backgrounds array
            this._backgrounds = this._backgrounds || [];
            this._backgrounds.push( background );

            // Set the base position for the new background
            var cols = 2;
            var col = ((this._backgrounds.length-1) % cols);
            var row = Math.floor((this._backgrounds.length-1)/cols);

            // Positioning
            background.position.x = this._activeSettings.size.width * col;//this._activeSettings.size.width - ResizeManager.viewportWidth()*1.7;
            background.position.y = -this._activeSettings.size.height * row;
            background.position.z = 0;

            // Rotation
            background.rotation.x = 0;
            background.rotation.y = 0;
            background.rotation.z = 0;
            
            // Keep track of original positioning
            this._originalBackgroundPositions = this._originalBackgroundPositions || [];
            this._originalBackgroundPositions.push({
                x: background.position.x,
                y: background.position.y,
                z: background.position.z,
            });
            this._startingBackgroundPositions = this._originalBackgroundPositions;

            // Set noise values for the background            
            var noiseValueX = (this._activeSettings.size.width/segmentsX)/100;
            var noiseValueY = (this._activeSettings.size.height/segmentsY)/100;
            var noiseValueZ = 2;
            var vertice;
            var random, randomPos;
            var duration = 0.2;
            var minDuration = 2;

            var timeline = new TimelineMax();

            var x, y, z;
            for (var verticeIndex in background.geometry.vertices) {
                vertice = background.geometry.vertices[ verticeIndex ];

                // Used to randomize values
                random = Math.random()*100;
                randomPos = (Math.random()*100)-50;

                // Check if current vertice is on an edge
                var isEdgeVerticeX = (Math.floor(Math.abs(vertice.x)) === Math.floor(this._activeSettings.size.width/2) );
                var isEdgeVerticeY = (Math.floor(Math.abs(vertice.y)) === Math.floor(this._activeSettings.size.height/2) );
                var isEdgeVertice = isEdgeVerticeX || isEdgeVerticeY;

                // Set different values based on vertice position (edge yes or no)
                if (!isEdgeVertice) {
                    x = noiseValueX;
                    y = noiseValueY;
                    z = noiseValueZ;
                } else {
                    x = 0;
                    y = 0;
                    z = noiseValueZ;
                }

                // Enable these for static background pattern
                timeline.add( new TweenMax.to( vertice, 0, {y:'+='+y*randomPos}), 0);
                timeline.add( new TweenMax.to( vertice, 0, {x:'+='+x*randomPos}), 0);
                timeline.add( new TweenMax.to( vertice, 0, {z:z*random}), 0);

                // Enable these for organic moving background pattern
                // timeline.add( new TweenMax.fromTo( vertice, Math.max(duration*random ,minDuration), {y:'+='+y*randomPos}, {delay: Math.random(), y:'-='+y*randomPos, repeat:-1, yoyo:true, ease:Quad.easeInOut}), 0);
                // timeline.add( new TweenMax.fromTo( vertice, Math.max(duration*random ,minDuration), {x:'+='+x*randomPos}, {delay: Math.random(), x:'-='+x*randomPos, repeat:-1, yoyo:true, ease:Quad.easeInOut}), 0);
                // timeline.add( new TweenMax.fromTo( vertice, Math.max(duration*random ,minDuration), {z:0}, {delay: Math.random(), z:z*random, repeat:-1, yoyo:true, ease:Quad.easeInOut}), 0);
                
            }

            // Add the current background to the scene
            this._scene.add( background );
            
        },

        _createLights: function() {

            // Ambient light
            this._ambientLight = new THREE.AmbientLight(this._activeSettings.lights.ambient.color);
            this._scene.add( this._ambientLight );


            // Directional light
            this._light = new THREE.DirectionalLight( this._activeSettings.lights.directional.color, this._activeSettings.lights.directional.intensity.start); //0xdfebff, 0.2
            this._light.position.set( this._activeSettings.lights.directional.x, this._activeSettings.lights.directional.y, this._activeSettings.lights.directional.z ); //0, 20, 200
            this._light.position.multiplyScalar(1.3);

            if (this._activeSettings.lights.directional.intensity.start !== this._activeSettings.lights.directional.intensity.end) {
                TweenMax.to( this._light, 2, {delay:1, intensity:this._activeSettings.lights.directional.intensity.end, ease:Quad.easeOut});
            }

            this._light.castShadow = true;

            this._light.shadow.bias = 0.0001;
            this._light.shadow.mapSize.width = 2048;
            this._light.shadow.mapSize.height = 2048;

            var d = this._activeSettings.lights.directional.size || 400;

            this._light.shadow.camera.left = -d;
            this._light.shadow.camera.right = d;
            this._light.shadow.camera.top = d;
            this._light.shadow.camera.bottom = -d;

            this._light.shadow.camera.far = 1000;

            this._scene.add(this._light);

            // var helper = new THREE.CameraHelper( this._light.shadow.camera );
            // this._scene.add( helper );

        },

        _render: function() {

            // Disable rendering when paused
            if (this._paused) return;

            /*
             *  Checks the camera position each frame
             *  Move seperate backgrounds based on camera position to create an infinite background pattern
             */
            var backgroundRelativePosition = {};
            for (var i = 0, limit = this._backgrounds.length; i < limit; i++) {
                this._backgrounds[ i ].geometry.colorsNeedUpdate = true;
                this._backgrounds[ i ].geometry.verticesNeedUpdate = true;

                backgroundRelativePosition.x = this._backgrounds[ i ].position.x - this._camera.position.x;
                backgroundRelativePosition.y = this._backgrounds[ i ].position.y - this._camera.position.y;

                if ( backgroundRelativePosition.y > this._activeSettings.size.height ) {
                    this._backgrounds[ i ].position.y -= this._activeSettings.size.height*2;
                }
                else if (backgroundRelativePosition.y < -this._activeSettings.size.height) {
                    this._backgrounds[ i ].position.y += this._activeSettings.size.height*2;
                }

                if ( backgroundRelativePosition.x > this._activeSettings.size.width ) {
                    this._backgrounds[ i ].position.x -= this._activeSettings.size.width*2;
                }
                else if (backgroundRelativePosition.x < -this._activeSettings.size.width) {
                    this._backgrounds[ i ].position.x += this._activeSettings.size.width*2;
                }

            }

            // Keeps the background in motion
            this._camera.position.y -= this._backgroundSpeed;

            // Render the actual scene
            this._renderer.render( this._scene, this._camera );

        },

        _scroll: function( scroll ) {
            
            // Disable the scroll when background is transitioning
            if (this._transitioning) return;

            // Set the scrollSpeed for the background
            // Make sure the background does not "jump" more then 100 pixels
            var scrollSpeed = 0;
            if (this._previousScrollPosition) {
                scrollSpeed = this._previousScrollPosition - ScrollManager.getScrollPositionY();
                scrollSpeed = Math.min(Math.max(scrollSpeed, -100), 100);
            }

            // Move camera based on scrollSpeed
            var startY = this._startingCameraPosition.y;
            TweenMax.to( this._camera.position, 0.1, {y:'+='+scrollSpeed, ease:Linear.easeNone});

            // keep track of previous ScrollPosition
            this._previousScrollPosition = ScrollManager.getScrollPositionY()

        },

        _moveBackground: function( position, duration, ease ) {

            // This function actually moves the camera position
            // This will create the illusion that the background is moving instead

            ease = ease || Power3.easeInOut;
            duration = duration || 2;
            position = position || {};//this._backgroundOriginalPos;
            
            position.x = position.x || 0;//this._backgroundOriginalPos.x;
            position.y = position.y || 0;//this._backgroundOriginalPos.y;
            position.z = position.z || 0;//this._backgroundOriginalPos.z;
            

            if (this._backgroundTransitionTimeline) this._backgroundTransitionTimeline.kill();

            this._transitioning = true;
            this._backgroundTransitionTimeline = new TimelineMax({
                onComplete: this._moveBackgroundCompleteHandler
            });

            this._startingBackgroundPositions.y = this._camera.position.y;

            this._startingCameraPosition.x += position.x;
            this._startingCameraPosition.y += position.y;
            this._startingCameraPosition.z += position.z;
            
            this._backgroundTransitionTimeline.add( new TweenMax.to( this._camera.position, duration, {x:'+='+position.x, y:'+='+position.y, z:'+='+position.z, ease:ease}), 0);

        },

        _pauseRendering: function() {

            this._paused = true;

        },

        _resumeRendering: function() {

            this._paused = false;

        },

        _moveBackgroundCompleteHandler: function() {

            this._transitioning = false;
            this._previousScrollPosition = ScrollManager.getScrollPositionY();

        },

        _scrollHandler: function( e ) {

            this._scroll();

        },

        _mouseMoveHandler: function( e ) {

            // Get the mouse position and normalize the values
            this._mouse = this._mouse || new THREE.Vector2();
            this._mouse.x = (e.clientX / ResizeManager.viewportWidth())*2-1;
            this._mouse.y = (e.clientY / ResizeManager.viewportHeight())*2+1;
            

            // Move the background slightly
            if (!this._transitioning) {
                var cameraPosX = this._startingCameraPosition.x + (this._mouse.x*50);
                TweenMax.to( this._camera.position, 2, {x:cameraPosX, ease:Quad.easeOut});
            }

            // Change the direction of the lighting
            // This gives more depth to the background pattern
            if (this._light) {
                var lightPosX = (this._mouse.x) * ResizeManager.viewportWidth()/10;
                var lightPosY = -(this._mouse.y-2) * ResizeManager.viewportHeight()/10;
                TweenMax.to( this._light.position, 3, {x:lightPosX, y:lightPosY, ease:Quad.easeOut});
            }

        },

        _backgroundSetSpeedHandler: function( speed ) {

            this._backgroundSpeed = speed;

        },

        _backgroundMoveHandler: function( e ) {

            this._moveBackground( e.position, e.duration, e.ease );

        },

        _backgroundPauseHandler: function() {
            
            this._pauseRendering();

        },  

        _backgroundResumeHandler: function() {
            
            this._resumeRendering();

        },

        _routeStartHandler: function(e) {

        }

    });
    
});