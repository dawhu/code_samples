--[[
 *  [ Corona SDK - Lua ]
 *  Project: KLM Layover with a local
 *  Url: https://layoverwithalocal.klm.com/
 *
 *  Net als de KLMLayoverApplicationRouter heb ik hier ook een uitbreiding gemaakt op de standaard baseScene van Composer.
 *  Deze BaseScene wordt door alle scenes in het project extend zodat de basis functionaliteit van alle files hetzelfde is.
 *  Alleen specifieke animaties etc hoeven nog toegevoegd te worden aan de extended views.
 */
]]

local BaseScene = {};

function BaseScene:new()

    local   _navigationOpenHandler,
            _navigationCloseHandler,
            _overlayOpenedHandler,
            _overlayClosedHandler;

    local scene = Composer.newScene();
          scene.name = 'BaseScene';

    local UI = {
        container = display.newGroup()
    };
    local Modules = {
        Background = require( Constants.Components.BACKGROUND_COMPONENT )
    };

    -- -----------------------------------------------------------------------------------------------------------------
    -- All code outside of the listener functions will only be executed ONCE unless 'composer.removeScene()' is called.
    -- -----------------------------------------------------------------------------------------------------------------

    -- local forward references should go here

    -- -------------------------------------------------------------------------------

    -- 'scene:create()'
    function scene:create( event )
        self.view:insert( UI.container );

        -- Log.info('base opts', event.params);
        self.params = event.params;

        -- Create colorOverlay for each scene
        UI.colorOverlay = Modules.Background:new( Constants.Colors.KLM_DARK_BLUE_2 );
        UI.colorOverlay.alpha = 0;
        self.view:insert( UI.colorOverlay );

        -- Initialize the scene here.
        -- Example: add display objects to 'sceneGroup', add touch listeners, etc.
        self:onInitialized( event.params );
    end


    function scene:setData(data)
        Log.info('BaseScene:setData please overwrite');
    end



    -- "scene:show()"
    function scene:show( event )
        local phase = event.phase

        if ( phase == 'will' ) then
            -- Called when the scene is still off screen (but is about to come on screen).
            self:beforeTransitionIn();

        elseif ( phase == 'did' ) then
            -- Called when the scene is now on screen.
            -- Insert code here to make the scene come alive.
            -- Example: start timers, begin animation, play audio, etc.
            self:transitionIn();
            self:activate();
        end
    end


    -- 'scene:hide()'
    function scene:hide( event )
        local phase = event.phase

        if ( phase == 'will' ) then
            -- Called when the scene is on screen (but is about to go off screen).
            -- Insert code here to 'pause' the scene.
            -- Example: stop timers, stop animation, stop audio, etc.
            self:deactivate();
            self:transitionOut();

            -- Fade out
            tween.to( self.view, shcc.router.DEFAULT_EFFECT_TIME/1000, {
                alpha = 0,
                transition = easing.outQuad
            });

        elseif ( phase == 'did' ) then
            -- Called immediately after scene goes off screen.
            self:transitionOutComplete();

            shcc.router.removeHiddenScene();
        end
    end


    -- 'scene:destroy()'
    function scene:destroy( event )

        -- Called prior to the removal of scene's view ('sceneGroup').
        -- Insert code here to clean up the scene.
        -- Example: remove display objects, save state, etc.

        klm.layover.navigation:removeEventListener( Constants.Events.NAVIGATION_OPEN, _navigationOpenHandler );
        klm.layover.navigation:removeEventListener( Constants.Events.NAVIGATION_CLOSE, _navigationCloseHandler );
        Global:removeEventListener( Constants.Events.OVERLAY_OPENED, _overlayOpenedHandler );
        Global:removeEventListener( Constants.Events.OVERLAY_CLOSED, _overlayClosedHandler );

        self:onDispose();

        -- Call dispose on all UI elements
        for key, object in pairs( UI ) do
            if #object > 0 then
                for i=1, #object do
                    if object[ i ]['dispose'] then
                        object[ i ]:dispose();
                        display.remove( object[ i ] );
                        object[ i ] = nil;
                    end
                end
                Log.info( #object );
                i = nil;
            end

            if object['dispose'] then
                object:dispose();
                display.remove( object );
            end
            key = nil;
            object = nil;
        end
        UI = {};

        -- Remove all modules
        for key, mod in pairs( Modules ) do
            key = nil;
            mod = nil;
        end
        Modules = nil;

        collectgarbage('collect');

        Log.error(scene.name, 'Memory: '..round(collectgarbage('count'))..' kb');
    end

    function scene:activate()
        for k,v in pairs(UI) do
            if v.name == Constants.Components.SCROLL_VIEW_COMPONENT then
                v:unlock();
            end
        end
        self:onActive();
    end

    function scene:deactivate()
        for k,v in pairs(UI) do
            if v.name == Constants.Components.SCROLL_VIEW_COMPONENT then
                v:lock();
            end
        end
        self:onInactive();
    end


    function scene:getUI()
        return UI;
    end

    function scene:getModules()
        return Modules;
    end

    function scene:showOverlay()
        tween.to( UI.colorOverlay, 0.3, {
            alpha = 0.05
        });
    end

    function scene:hideOverlay()
        tween.to( UI.colorOverlay, 0.3, {
            alpha = 0
        });
    end

    -- Below functions should be overriden in the extended scenes
    function scene:onInitialized()
        Log.warn( scene.name, 'Override onInitialized' );
    end

    function scene:beforeTransitionIn()
        Log.warn( scene.name, 'Override beforeTransitionIn' );
    end

    function scene:transitionIn()
        Log.warn( scene.name, 'Override transitionIn' );
    end

    function scene:transitionOut()
        Log.warn( scene.name, 'Override transitionOut' );
    end

    function scene:transitionOutComplete()
        Log.warn( scene.name, 'Override transitionOutComplete' );
    end

    function scene:onActive()
        Log.warn( scene.name, 'Override onActive' );
    end

    function scene:onInactive()
        Log.warn( scene.name, 'Override onInactive' );
    end

    function scene:onDispose()
        Log.warn( scene.name, 'Override onDispose' );
    end

    -- -------------------------------------------------------------------------------
    -- Functions to handle menu open and close
    function _navigationOpenHandler( e )
        -- Deactivate scene when menu is opened
        scene:deactivate();
        scene:showOverlay();

        -- Kill active tween on container
        tween.kill( UI.container );

        tween.to( UI.container, klm.layover.navigation.OPEN_DURATION, {
            x = klm.layover.navigation:getWidth(),
            transition = klm.layover.navigation.OPEN_TRANSITION
        });
    end

    function _navigationCloseHandler( e )
        -- Activate scene when menu is opened
        scene:activate();
        scene:hideOverlay();

        -- Kill active tween on container
        tween.kill( UI.container );

        tween.to( UI.container, klm.layover.navigation.CLOSE_DURATION, {
            x = 0,
            transition = klm.layover.navigation.CLOSE_TRANSITION
        });
    end

    function _overlayOpenedHandler( e )
        scene:deactivate();
    end

    function _overlayClosedHandler( e )
        scene:activate();
    end



    -- -------------------------------------------------------------------------------

    -- Listener setup
    scene:addEventListener( 'create', scene );
    scene:addEventListener( 'show', scene );
    scene:addEventListener( 'hide', scene );
    scene:addEventListener( 'destroy', scene );

    klm.layover.navigation:addEventListener( Constants.Events.NAVIGATION_OPEN, _navigationOpenHandler );
    klm.layover.navigation:addEventListener( Constants.Events.NAVIGATION_CLOSE, _navigationCloseHandler );
    Global:addEventListener( Constants.Events.OVERLAY_OPENED, _overlayOpenedHandler );
    Global:addEventListener( Constants.Events.OVERLAY_CLOSED, _overlayClosedHandler );

    -- -------------------------------------------------------------------------------

    return scene

end

return BaseScene;